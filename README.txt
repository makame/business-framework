npm install --save babel babel-plugin-transform-react-jsx babel-register express fs lodash.defaults react react-dom require-dir

npm install --save browserify envify gulp gulp-util gulp-uglify gulp-react gulp-clean gulp-rename gulp-browserify
npm install --save-dev gulp-plumber

node app.js

supervisor --watch . --ignore public/. -e js,jsx -x /bin/bash run.sh

supervisor --instant-kill --watch . --ignore public/. -e js,jsx app.js
gulp
lsof -t -i tcp:3000 | xargs kill
