app.get('/', function (req, res) {
  Fiber(function() {
    res.render('basic.jsx', { page: 'home', data: {} });
  }).run();
})

app.get('/login', function (req, res) {
  Fiber(function() {
    res.render('basic.jsx', { page: 'login', data: {} });
  }).run();
})

app.get('/:module/:par', function (req, res) {
  var n = req.session.views || 0;
  req.session.views = ++n;
  console.log(req.session.views);

  Fiber(function() {

    Clients.table().insert({first_name: guid(), last_name: guid()}).run();
    Clients.table().insert({first_name: guid(), last_name: guid()}).run();

    var view = global[req.params.module + 'View'];
    if (view) {
      switch (view.type) {
        case 'grid':
        res.render('basic.jsx', { page: 'grid', view: view, data: {par: req.params.par, name: req.params.module} });
        break;
        default:
        res.render('basic.jsx', { page: 'basic', view: view, data: {message: 'hello'} });
      }
    }
    else {
      res.send('404');
    }
  }).run();
})

app.get('/sessions/new', function(req, res) {
  console.log(req.session.currentUser);
});

app.post('/sessions', function(req, res) {
  // Найти пользователя и выставить currentUser
});

app.delete('/sessions', function(req, res) {
  if (req.session) {
    req.session.destroy(function() {});
  }
  res.redirect('/sessions/new');
})
