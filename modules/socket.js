sock.on('connection', function(socket) {

  connected.push({socket: socket, subs: {}});

  socket.on('test', function(data) {
    var n = socket.request.session.views || 0;
    socket.request.session.views = ++n;
    socket.emit('message', socket.request.session.views);
  });

  socket.on('subscribe_view', function(data) {
    var name = data.name
    var order = data.order || 'id'
    var order_desc = data.order_desc || false
    var limit = data.limit || 10
    var skip = data.skip || 0
    var filters = data.filters || []

    Fiber(function() {
      var view = global[name + 'View'];
      for (var i = 0; i < connected.length; i++) {
        if (connected[i].socket === socket) {
          socket.emit('view_changes', {name: name, data: {fields: view.fields, filters: view.filters, state: "preinitializing", order: order, order_desc: order_desc, limit: limit, skip: skip}});

          var order_r = r.asc(order)

          if (order_desc) {
            order_r = r.desc(order)
          }

          var source = view.source().orderBy({index: order_r})

          _.each(filters, function(filter, index) {
              if (filter.type == 'regexp' && filter.value) {
                source = source.filter(function(doc) {
                  return doc(filter.field).match(filter.value)
                })
              }
          })

          source.count().run().then(function(res) {
            socket.emit('view_changes', {
              name: name,
              data: {
                count: res,
                state: "additional",
              },
            })
          })

          source = source.limit(limit)

          // source = source.slice(skip, skip + limit)

          // source = source.skip(skip)

          if (view.map) {
            source = source.map(view.map)
          }

          if (connected[i].subs[name + 'View']) {
            connected[i].subs[name + 'View'].cursor.close();
          }

          var back = null;
          source.changes({squash: 0.05, includeInitial: true, includeStates: true}).run().then(function(cursor) {
            connected[i].subs[name + 'View'] = {
              cursor: cursor
            };

            cursor.each(function(err, item) {
              socket.emit('view_changes', {
                name: name,
                data: item,
              });
            })
          });
          break;
        }
      }
    }).run();
  });

  socket.on('desubscribe_view', function(name) {
    var view = global[name + 'View'];
    for (var i = 0; i < connected.length; i++) {
      if (connected[i].socket === socket) {
        if (connected[i].subs[name + 'View']) {
          connected[i].subs[name + 'View'].cursor.close();
          delete connected[i].subs[name + 'View'];
        }
        break;
      }
    }
    socket.emit('message', socket.request.session.views);
  });

  socket.on('disconnect', function(){
    console.log('disconnect');
    for (var i = 0; i < connected.length; i++) {
      if (connected[i].socket === socket) {
        for (var prop in connected[i].subs) {
          connected[i].subs[prop].cursor.close();
        }
        connected.splice(i, 1);
        break;
      }
    }
  });

});
