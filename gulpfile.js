var gulp = require('gulp');
var plumber = require('gulp-plumber');
var react = require('gulp-react');
// var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var browserify = require('gulp-browserify');
var order = require("gulp-order");

gulp.task('default', function() {
  // Listen to every JS file in frontend/javascript
  return gulp.src(['lib/**/*.jsx', 'client/**/*.jsx'])
  .pipe(order([
    'lib/**/*.jsx',
    'client/**/Lib/*.jsx',
    'client/**/*.jsx',
  ], {base: '.'}))
  .pipe(plumber())
  // Turn React JSX syntax into regular javascript
  .pipe(react())
  .pipe(concat('client.raw.js'))
  // Optimize each JavaScript file
  // .pipe(uglify())
  // Output each optimized .min.js file into the build/javascript/ dir
  .pipe(gulp.dest('public/'))
  .pipe(browserify({transform: ['envify']}))
  .pipe(rename('client.js'))
  .pipe(gulp.dest('public/'))
  // .pipe(uglify())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('public/'));
});

gulp.watch('client/**', function(event) {
  gulp.run('default');
});

gulp.watch('lib/**', function(event) {
  gulp.run('default');
});
