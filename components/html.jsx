var React = require('react');

var Html = React.createClass({
  render: function () {
    return (
      <html>
        <head>
          <title>{this.props.props.title}</title>
          <link rel='stylesheet' type='text/css' href='../../../../../style.css'/>
        </head>
        <body>
            <div id="window-container"/>
            <div id="loading-container"/>
            <div id='view-server' dangerouslySetInnerHTML={{__html: this.props.body}} />
            <script src="../../../../../jquery.js"></script>
            <script src="../../../../../socket.io/socket.io.js"></script>
            <script src="../../../../../client.js"></script>
        </body>
      </html>
    );
  }
});

module.exports = Html;
