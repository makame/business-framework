var React = require('react');

var Basic = React.createClass({
  render: function () {
    var props = {};

    for (var property in this.props) {
      if (this.props.hasOwnProperty(property)) {
        props[property] = this.props[property];
      }
    }

    if (this.props.view) {
      props.source = this.props.view.data();
    }

    return (
      <div>
        <div id="props" style={{display: 'none'}}>
          {JSON.stringify(props, null, 2)}
        </div>
        <div id="view-client">
        </div>
      </div>
    );
  }
});

module.exports = Basic;
