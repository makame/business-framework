function View(init_data) {
  this._init_data = init_data;
  this.source = init_data.source;
  this.transform = init_data.transform;
  this.type = init_data.type;
  this.fields = init_data.fields;
  this.filters = init_data.filters;
  this.map = init_data.map;
}

View.prototype.log = function() {
  console.log(this._init_data);
}

View.prototype.data = function() {
  var raw = this._init_data.source();

  if (this.map) {
    raw = raw.map(this.map)
  }

  var source = {
    data: function() {
      var future = new Future;
      raw.run().then(function(rows) {
        future.return(rows);
      });
      return future;
    }().wait(),
    fields: this.fields
  }

  return source;
}

var Future = require('fibers/future')

clientsView = new View({
  name: 'clients',
  label: 'Clients',
  type: 'grid',
  params: null,
  map: function(doc) {
    return doc.merge({
      contacts: function() {
        var future = new Future;
        Contacts.table().filter({contact_id: doc.id}).run().then(function(rows) {
          future.return({data: rows, fields: Contacts.fields});
        });
        return future;
      }().wait()
    })
  },
  source: function(params) {
    return Clients.table();
  },
  fields: {
    id: {
      label: 'ID',
      name: 'id',
      type: 'string',
      sortable: true,
    },
    first_name: {
      label: 'First Name',
      name: 'first_name',
      type: 'string',
      sortable: true,
    },
    last_name: {
      label: 'Last Name',
      name: 'last_name',
      type: 'string',
      sortable: true,
    },
    contacts: {
      label: 'Contacts',
      name: 'contacts',
      type: 'refs',
    },
  },
  filters: [
    {
      label: 'First Name Contains',
      field: 'first_name',
      type: 'regexp',
    },
    {
      label: 'First Name is',
      field: 'first_name',
      type: 'empty',
    },
  ],
  // onclick: function(row) {
  //   OpenView(ClientView, row.id);
  // }
})

clientView = new View({
  name: 'client',
  label: 'Client',
  type: 'detail',
  params: ['id'],
  source: function(params) {
    // var fields = [Clients.fields.id, Clients.fields.first_name, Clients.fields.last_name]
    //
    // var client = Clients.select(fields).where(WhereEq(Clients.fields.id, params.id)).query();
    //
    // return client;
  },
  // onclick: function() {
  //   OpenView(ClientView);
  // }
})
