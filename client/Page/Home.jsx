homePage = React.createClass({
  render: function () {
    var popup = (<ConfidentialPopup/>)
    var items = (<ItemsPopup/>)
    console.log(this.props.data);
    return (
      <div>
      <div className="top-menu">
        <div className="header">
          Your Company Name
        </div>
        <div className="items">
          <div className="item">
            Orders
          </div>
          <div className="item">
            Clients
          </div>
          <div className="item">
            Products
          </div>
        </div>
        <PopupElement html={items} position="bottom center" offset={-0} className="item more" on="click" back={true}>
          <span className="fa fa-bars"></span>
          Menu
        </PopupElement>
        <div className="space">
        </div>
        <PopupElement html={popup} position="bottom left" offset={-0} className="item" on="click" back={true}>
          <span className="fa fa-cog"></span>
          System
        </PopupElement>
      </div>
      <div>
        Hello
      </div>
    </div>
    );
  }
});

ItemsPopup = React.createClass({
  render() {
    return (
      <div className="popup-menu">
        <div className="item">
          Orders
        </div>
        <div className="item">
          Clients
        </div>
        <div className="item">
          Products
        </div>
      </div>
    )
  }
});

ConfidentialPopup = React.createClass({
  render() {
    var popup = (<ConfidentialModal/>)
    return (
      <div className="padding-vertical-2 padding-horizontal-2">
        <p>
          All information will stay confidential and will not be transfered to the third parties. We do not store any of you application data after the moderation process. We do not store any logs IP's or any of your provider information.
        </p>
        <ModalElement html={popup} className="a">
          Privacy Policy
        </ModalElement>
      </div>
    )
  }
});

ConfidentialModal = React.createClass({
  render() {
    return (
      <div className="container litle padding-vertical-4 padding-horizontal-4">
        <div>
          <p>
            All information will stay confidential and will not be transfered to the third parties. We do not store any of you application data after the moderation process. We do not store any logs IP's or any of your provider information.
          </p>
          <div>
            <a>Privacy Policy</a>
          </div>
        </div>
      </div>
    )
  }
});
