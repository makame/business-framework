gridPage = React.createClass({
  mixins: [LinkedStateMixin, BusinessData],

  getInitialState: function() {
    return {
      grid_data: {
        loading_view: true,
        data: [],
        fields: [],
        order: null,
        order_desc: false,
        skip: 0,
        limit: 10,
        filters: [],
        count: 0,
      },
      page: 1,
      per_page: 10,
      selected_row: [],
    }
  },

  do_subscribe: function() {
    this.state.grid_data.limit = this.state.page * this.state.per_page
    this.state.grid_data.skip = (this.state.page - 1) * this.state.per_page
    this.setState(this.state.grid_data)
    this.subscribe_view(this.props.data.name, this.receiveViewData, this.linkState('grid_data'))
  },

  go_page: function(page) {
    if (page < 1) {
      return
    }

    if (page > Math.ceil(this.state.grid_data.count / this.state.per_page)) {
      return
    }

    if (this.state.grid_data.loading_view) {
      return
    }

    this.state.page = page
    this.setState({grid_data: this.state.grid_data, page: this.state.page})
    this.do_subscribe()
  },

  load_more: function() {
    if (this.state.per_page > this.state.grid_data.count) {
      return
    }

    if (this.state.grid_data.loading_view) {
      return
    }

    this.state.per_page = this.state.per_page + 10
    this.state.page = 1
    this.state.grid_data.limit = this.state.page * this.state.per_page
    this.state.grid_data.skip = (this.state.page - 1) * this.state.per_page
    this.setState({per_page: this.state.per_page, page: this.state.page, grid_data: this.state.grid_data})
    this.do_subscribe()
  },

  load_less: function() {
    if (this.state.per_page <= 10) {
      return
    }

    if (this.state.grid_data.loading_view) {
      return
    }

    this.state.per_page = this.state.per_page - 10
    this.state.page = 1
    this.state.grid_data.limit = this.state.page * this.state.per_page
    this.state.grid_data.skip = (this.state.page - 1) * this.state.per_page
    this.setState({per_page: this.state.per_page, page: this.state.page, grid_data: this.state.grid_data})
    this.do_subscribe()
  },

  isAltDown: false,

  componentDidMount: function() {
    var self = this
    document.onkeydown = function(e) {
      switch (e.keyCode) {
        case 18:
        self.isAltDown = true
        break;
        case 37:
        if (self.isAltDown) {
          self.go_page(self.state.page - 1)
        }
        break;
        case 39:
        if (self.isAltDown) {
          self.go_page(self.state.page + 1)
        }
        break;
        case 38:
        if (self.isAltDown) {
          self.load_less()
        }
        break;
        case 40:
        if (self.isAltDown) {
          self.load_more()
        }
        break;
      }
    }

    document.onkeyup = function(e) {
      switch (e.keyCode) {
        case 18:
        self.isAltDown = false
        break;
      }
    }

    this.do_subscribe()
  },

  render: function () {
    var self = this
    var items = (<ItemsPopup/>)

    var onClick = function() {
      self.do_subscribe()
    }

    var table_heads = [];

    table_heads.push(
      <td key='checkbox'></td>
    )

    _.each(this.state.grid_data.fields, function(field) {

      if (field.sortable) {
        var order_icon = null

        if (self.state.grid_data.order == field.name && self.state.grid_data.order_desc) {
          order_icon = (
            <span className="fa fa-sort-amount-desc"></span>
          )
        }
        else if (self.state.grid_data.order == field.name && !self.state.grid_data.order_desc) {
          order_icon = (
            <span className="fa fa-sort-amount-asc"></span>
          )
        }

        var reOrder = function() {
          if (self.state.grid_data.order == field.name && !self.state.grid_data.order_desc) {
            self.state.grid_data.order_desc = true
          }
          else {
            self.state.grid_data.order_desc = false
          }
          self.state.grid_data.order = field.name

          self.setState(self.state.grid_data)

          self.do_subscribe()
        }

        table_heads.push(
          <td className="pointer" key={field.name} onClick={reOrder}>{field.label} {order_icon}</td>
        )
      }
      else {
        table_heads.push(
          <td key={field.name}>{field.label}</td>
        )
      }
    })

    var table_items = [];
    var data = this.state.grid_data.data

    // if (this.state.grid_data.data.length > this.state.grid_data.skip) {
    data = data.slice(this.state.grid_data.skip, this.state.grid_data.skip + this.state.per_page)
    // }

    _.each(data, function(item, index) {
      var tds = []

      var linkCheck = {
        value: item.checkbox,
        requestChange: function(newValue) {
          self.state.grid_data.data[index].checkbox = newValue;
          self.setState(self.state.grid_data.data[index].checkbox);
        }
      }

      tds.push(<td key='checkbox'><input type='checkbox' linkState={linkCheck}></input></td>)

      _.each(self.state.grid_data.fields, function(field) {
        if (field.type == 'string') {
          tds.push(<td key={field.name}>{item[field.name]}</td>)
        }
        else if (field.type == 'refs') {
          //{item[field.name].data.length}
          tds.push(<td key={field.name} className="ellipsis">{item[field.name].data.length}</td>)
        }
      })

      table_items.push(
        <tr key={item.id}>
          {tds}
        </tr>
      )
    })

    var table = (
      <table className="table container p95 center" key="table">
        <thead>
          <tr>
            {table_heads}
          </tr>
        </thead>
        <tbody>
          {table_items}
        </tbody>
      </table>
    )

    var filters = []

    _.each(self.state.grid_data.filters, function(item, index) {
      if (item.type == "regexp") {

        var link = {
          value: item.value,
          requestChange: function(newValue) {
            item.value = newValue;
            self.setState(self.state.grid_data);
          }
        }

        filters.push(
          <tr key={index}>
            <td className="type-1 align-right width-1 nowrap">{item.label}</td>
            <td className="type-1"><Input type="text" linkState={link} onEnter={self.do_subscribe}/></td>
          </tr>
        )
      }
    })

    var stat = (
      <div className="container block align-center margin-top-2">
        Showed {self.state.grid_data.data.length} of {self.state.grid_data.count} & Skiped {self.state.grid_data.skip}
      </div>
    )

    var pagination_item = []

    var max_page = Math.ceil(self.state.grid_data.count / self.state.per_page)

    if (self.state.page > 1) {
      var goBack = function() {
        self.go_page(self.state.page - 1)
      }

      pagination_item.push(<div onClick={goBack} key="back">(Alt + Left)</div>)
    }

    if (self.state.page > 1) {
      var goFirst = function() {
        self.go_page(1)
      }

      pagination_item.push(<div onClick={goFirst} key="first">1</div>)
    }

    if (self.state.page > 2) {
      pagination_item.push(<div className="disabled" key="pre">..</div>)
    }

    pagination_item.push(<div className="active disabled" key="current">{self.state.page}</div>)

    if (self.state.page < max_page - 1) {
      pagination_item.push(<div className="disabled" key="after">..</div>)
    }

    if (self.state.page < max_page) {
      var goMax = function() {
        self.go_page(max_page)
      }

      pagination_item.push(<div onClick={goMax} key="max">{max_page}</div>)
    }

    if (self.state.page < max_page) {
      var goNext = function() {
        self.go_page(self.state.page + 1)
      }

      pagination_item.push(<div onClick={goNext} key="next">(Alt + Right)</div>)
    }

    var pagination = (
      <div className="buttons container block align-center margin-top-2">
        {pagination_item}
      </div>
    )

    if (max_page == 1) {
      pagination = null
    }

    var load_more_func = this.load_more
    var load_more_class = ""

    if (this.state.grid_data.loading_view) {
      load_more_func = null
      load_more_class = "disabled"
    }

    var load_more = (
      <div className="buttons container block align-center margin-top-2">
        <div className={load_more_class} onClick={load_more_func}>Load More</div>
      </div>
    )

    if (this.state.per_page > this.state.grid_data.count || this.state.page > 1) {
      load_more = null
    }

    var popup = (<ConfidentialPopup/>)

    return (
      <div className="padding-bottom-2">
        <div className="top-menu container p90 block center">
          <div className="header">
            Your Company Name
          </div>
          <div className="items-desktop">
            <div className="item">
              Orders
            </div>
            <div className="item">
              Clients
            </div>
            <div className="item">
              Products
            </div>
          </div>
          <div className="items-mobile">
            <PopupElement html={items} position="bottom center" offset={-0} className="item" on="click" back={true}>
              <span className="fa fa-bars"></span>
              Menu
            </PopupElement>
          </div>
          <div className="space">
          </div>
          <PopupElement html={popup} position="bottom left" offset={-0} className="item" on="click" back={true}>
            <span className="fa fa-cog"></span>
            System
          </PopupElement>
        </div>

        <Accord header="Filters" icon="filter">
          <div className="padding-horizontal-2 padding-vertical-1">
            <table className="table">
              <tbody>
                {filters}
              </tbody>
            </table>
          </div>
        </Accord>

        {table}

        {stat}

        {load_more}

        {pagination}
      </div>
    );
  }
});

ItemsPopup = React.createClass({
  render() {
    return (
      <div className="popup-menu">
        <div className="item">
          Orders
        </div>
        <div className="item">
          Clients
        </div>
        <div className="item">
          Products
        </div>
      </div>
    )
  }
});

ConfidentialPopup = React.createClass({
  render() {
    var popup = (<ConfidentialModal/>)
    return (
      <div className="padding-vertical-2 padding-horizontal-2">
        <p>
          All information will stay confidential and will not be transfered to the third parties. We do not store any of you application data after the moderation process. We do not store any logs IP's or any of your provider information.
        </p>
        <ModalElement html={popup} className="a">
          Privacy Policy
        </ModalElement>
      </div>
    )
  }
});

ConfidentialModal = React.createClass({
  render() {
    return (
      <div className="container litle padding-vertical-4 padding-horizontal-4">
        <div>
          <p>
            All information will stay confidential and will not be transfered to the third parties. We do not store any of you application data after the moderation process. We do not store any logs IP's or any of your provider information.
          </p>
          <div>
            <a>Privacy Policy</a>
          </div>
        </div>
      </div>
    )
  }
});
