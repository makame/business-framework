var React = require('react');

Stater = React.createClass({
  getCurrentPage: function () {
    console.log(this.props);

    var varialable = window[this.props.page + 'Page'];
    if (varialable) {
      var page = React.createElement(varialable, {source: this.props.source, data: this.props.data})
      return page;
    }
    else {
      return 'Not Found Module';
    }
  },

  render: function () {
    return (
      <div>
        {this.getCurrentPage()}
      </div>
    );
  }
});
