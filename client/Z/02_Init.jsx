var ReactDOM = require('react-dom');

ReactDOM.render(<WindowContainer/>, document.getElementById('window-container'));

ReactDOM.render(<LoadingContainer/>, document.getElementById('loading-container'));

var props = JSON.parse(document.getElementById("props").innerHTML);
ReactDOM.render(<Stater page={props.page} source={props.source} data={props.data}/>, document.getElementById('view-client'));
