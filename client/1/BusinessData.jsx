view_subs = {}

socket.on('view_changes', function(data) {
  subs = view_subs[data.name];
  if (view_subs[data.name]) {
    view_subs[data.name].func(data.data, view_subs[data.name].valueLink);
  }
});

BusinessData = {

  subscribe_view: function(name, func, valueLink) {
    var temp = valueLink.value
    temp.loading_view = true
    loadingService.show()
    valueLink.requestChange(temp)

    view_subs[name] = {func: func, valueLink: valueLink}
    data = {
      name: name,
      order: valueLink.value.order,
      skip: valueLink.value.skip,
      limit: valueLink.value.limit,
      filters: valueLink.value.filters,
      order_desc: valueLink.value.order_desc,
    }
    socket.emit('subscribe_view', data);
  },

  desubscribe_view: function(name) {
    subs = view_subs[name]
    if (view_subs[name]) {
      delete view_subs[name]
    }
    socket.emit('desubscribe_view', {name: name});
  },

  orderViewData: function(temp) {
    temp.data = _.sortBy(temp.data, function(item) {
      return item[temp.order]
    })
    if (temp.order_desc) {
      temp.data = temp.data.reverse()
    }
    return temp
  },

  receiveViewData: function(data, valueLink) {
    if (data.state == 'preinitializing') {
      var temp = valueLink.value
      temp.fields = data.fields
      temp.order = data.order
      temp.skip = data.skip
      temp.limit = data.limit
      temp.order_desc = data.order_desc
      temp.loading_view = true
      loadingService.show()
      temp.filters = _.map(data.filters, function(item, index) {
        return _.extend(item, valueLink.value.filters[index])
      })
      valueLink.requestChange(temp)
    }
    else if (data.state == 'initializing') {
      var temp = valueLink.value
      temp.temp_data = []
      // temp.data = []
      temp.loading_view = true
      loadingService.show()
      valueLink.requestChange(temp)
    }
    else if (data.state == 'additional') {
      var temp = valueLink.value
      temp.count = data.count
      valueLink.requestChange(temp)
    }
    else if (data.state == 'ready') {
      var temp = valueLink.value
      temp.loading_view = false
      loadingService.hide()
      temp.data = temp.temp_data
      temp = this.orderViewData(temp)
      valueLink.requestChange(temp)
    }
    else {
      if (!!data.new_val && !data.old_val) {
        var temp = valueLink.value
        if (temp.loading_view) {
          var finded = false
          for (var i = 0; i < valueLink.value.data.length; i++) {
            if (!!temp.temp_data[i] && temp.temp_data[i].id == data.new_val.id) {
              temp.temp_data[i] = _.extend(data.new_val, temp.temp_data[i])
              finded = true
              break;
            }
          }
          if (!finded) {
            temp.temp_data.push(data.new_val)
          }
        }
        else {
          temp.data.push(data.new_val)
          temp = this.orderViewData(temp)
        }
        valueLink.requestChange(temp)
      }
      else if (!!data.new_val && !!data.old_val) {
        var temp = valueLink.value
        if (temp.loading_view) {
          for (var i = 0; i < valueLink.value.data.length; i++) {
            if (temp.temp_data[i].id == data.new_val.id) {
              temp.temp_data[i] = _.extend(data.new_val, temp.temp_data[i])
              break;
            }
          }
          valueLink.requestChange(temp)
        }
        else {
          for (var i = 0; i < valueLink.value.data.length; i++) {
            if (temp.data[i].id == data.new_val.id) {
              temp.data[i] = _.extend(data.new_val, temp.data[i])
              break;
            }
          }
          temp = this.orderViewData(temp)
          valueLink.requestChange(temp)
        }
      }
      else if (!data.new_val && !!data.old_val) {
        var temp = valueLink.value
        if (temp.loading_view) {
          for (var i = 0; i < temp.temp_data.length; i++) {
            if (temp.temp_data[i].id == data.old_val.id) {
              temp.temp_data.splice(i, 1)
              valueLink.requestChange(temp)
              break;
            }
          }
        }
        else {
          for (var i = 0; i < temp.data.length; i++) {
            if (temp.data[i].id == data.old_val.id) {
              temp.data.splice(i, 1)
              valueLink.requestChange(temp)
              break;
            }
          }
        }
      }
    }
  },
}
