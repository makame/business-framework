Accord = React.createClass({

  getInitialState: function() {
    return {
      show: false,
    }
  },

  toggleAccord: function(event) {
    this.state.show = !this.state.show
    this.setState(this.state)
  },

  render() {
    var status_icon = (
      <Icon name="plus-square-o"/>
    )

    if (this.state.show) {
      status_icon = (
        <Icon name="minus-square-o"/>
      )
    }

    var child = null

    if (this.state.show) {
      child = (
        <div id="child" className="accord">
          {this.props.children}
        </div>
      )
    }

    return (
      <div className="accord container block">
        <div className="accord-header align-right padding-horizontal-2 padding-vertical-1 pointer" onClick={this.toggleAccord}>
          {status_icon}
          {this.props.header}
          <Icon name={this.props.icon}/>
        </div>
        {child}
      </div>
    )
  }
});
