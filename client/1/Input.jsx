Input = React.createClass({
  handleChange: function(e) {
    this.props.linkState.requestChange(e.target.value);
  },

  onKeyUp: function(e) {
    if (e.which == 13) {
      if (this.props.onEnter) {
        this.props.onEnter()
      }
    }
  },

  render() {
    return (
      <input type={this.props.type} value={this.props.linkState.value} onChange={this.handleChange} onKeyUp={this.onKeyUp}/>
    )
  }
});
