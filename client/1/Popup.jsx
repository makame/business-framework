popupService = {};
modalService = {};

PopupElement = React.createClass({

  getInitialState: function() {
    return {guid: guid()}
  },

  getDefaultProps: function() {
    return {on: 'hover', back: false, html: '', position: 'top center', popupClassName: '', offset: 9}
  },

  show: function(e) {
    var element = $(ReactDOM.findDOMNode(this));
    if (this.props.element) {
      element = $(ReactDOM.findDOMNode(this.props.element));
    }

    popupService.showPopup({html: this.props.html, guid: this.state.guid, className: this.props.popupClassName, offset: this.props.offset}, element, this.props.back, this.props.position);
  },

  hide: function(e) {
    popupService.hidePopup(this.state.guid);
  },

  componentWillReceiveProps: function(nextProps) {
    this.props = nextProps;
    if (nextProps.on) {
      if (nextProps.show) {
        this.show();
      }
      else {
        this.hide();
      }
    }
    this.setState({on: nextProps.on});
  },

  componentDidMount: function() {
    this.setState({on: this.props.on});
  },

  render: function () {
    if (this.state.on == "manual") {
      return (<div className={this.props.className}>{this.props.children}</div>);
    }
    else if (this.state.on == "click") {
      return (<div onClick={this.show} onTouchStart={this.show} className={this.props.className}>{this.props.children}</div>);
    }
    else if (this.state.on == "hover") {
      return (<div onMouseOver={this.show} onMouseOut={this.hide} onTouchStart={this.show} onTouchEnd={this.hide} className={this.props.className}>{this.props.children}</div>);
    }
    else {
      return null;
    }
  }

});

ModalElement = React.createClass({

  guid: guid(),

  show: function(e) {
    e.stopPropagation();
    modalService.showModal({html: this.props.html, guid: this.guid, offset: this.props.offset});
  },

  hide: function(e) {
    modalService.hideModal(this.guid);
  },

  render: function () {
    return (<div onClick={this.show} onTouchStart={this.show} className={this.props.className}>{this.props.children}</div>);
  }

});

WindowContainer = React.createClass({

  componentWillMount(){
    var self = this;

    popupService.showPopup = function(popup, element, back, position) {
      if (!back) {
        back = false;
      }

      var offset = 9;

      if (popup && popup.offset != null) {
        offset = popup.offset;
      }

      self.setState({popup: popup, element, back, position, offset: offset});
    };

    popupService.hidePopup = function(guid) {
      if (guid) {
        if (self.state.popup && self.state.popup.guid == guid) {
          self.state.popup = null;
          self.setState({popup: self.state.popup, element: null});
        }
      }
      else {
        self.state.popup = null;
        self.setState({popup: self.state.popup, element: null});
      }
    };

    modalService.showModal = function(modal) {
      self.setState({modal: modal});
    };

    modalService.hideModal = function(guid) {
      self.state.modal = null;
      self.setState({modal: self.state.modal});
    };
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true
  },

  hideModal: function(e) {
    this.state.modal = null;
    this.setState({modal: this.state.modal});
  },

  hidePopup: function(e) {
    this.state.popup = null;
    this.setState({popup: this.state.popup, element: null});
  },

  hideWindow: function(e) {
    this.state.window = null;
    this.setState({window: this.state.window, element: null});
  },

  getInitialState: function() {
    return {modal: null, popup: null, element: null, padding_window: 10, position: 'top center', offset: 9};
  },

  componentDidUpdate: function() {
    if (this.state.modal) {
      var modal = $(ReactDOM.findDOMNode(this.refs.modal));

      var min_x = $(window).scrollLeft();
      var min_y = $(window).scrollTop();

      var max_x = $(window).width() + $(window).scrollLeft();
      var max_y = $(window).height() + $(window).scrollTop();

      var modal_height = modal.outerHeight();
      var modal_width = modal.outerWidth();

      var top = min_y + (max_y - min_y) / 2 - modal_height / 2;
      var left = min_x + (max_x - min_x) / 2 - modal_width / 2;

      modal.offset({top: top, left: left});
    }

    if (this.state.popup) {
      var popup = $(ReactDOM.findDOMNode(this.refs.popup));
      if (popup) {
        var element = this.state.element;

        var min_x = $(window).scrollLeft();
        var min_y = $(window).scrollTop();

        var max_x = $(window).width() + $(window).scrollLeft();
        var max_y = $(window).height() + $(window).scrollTop();

        var this_y = element.offset().top;
        var this_x = element.offset().left;

        var this_height = element.outerHeight();
        var this_width = element.outerWidth();

        var popup_height = popup.outerHeight();
        var popup_width = popup.outerWidth();

        var top = this_y;
        var left = this_x;

        var want_y = 'auto';
        var want_x = 'auto';

        var result_y = 'auto';
        var result_x = 'auto';
        var result_alter_y = 'center';

        if (this.state.position == 'top center' || this.state.position == 'top') {
          top = this_y - popup_height - this.state.offset;
          left = this_x + this_width / 2 - popup_width / 2;

          result_y = "top";
          result_x = "center";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y + this_height + this.state.offset;
            result_y = "bottom";
          }
        }

        if (this.state.position == 'top left') {
          top = this_y - popup_height - this.state.offset;
          left = this_x - popup_width + 22 + this_width / 2;

          result_y = "top";
          result_x = "left";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top < this_height - this.state.padding_window) {
            top = this_y + this_height + this.state.offset;
            result_y = "bottom";
          }
        }

        if (this.state.position == 'top right') {
          top = this_y - popup_height - this.state.offset;
          left = this_x + this_width - 22 - this_width / 2;

          result_y = "top";
          result_x = "right";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top < this_height - this.state.padding_window) {
            top = this_y + this_height + this.state.offset;
            result_y = "bottom";
          }
        }

        if (this.state.position == 'bottom center' || this.state.position == 'bottom') {
          top = this_y + this_height + this.state.offset;
          left = this_x + this_width / 2 - popup_width / 2;

          result_y = "bottom";
          result_x = "center";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.offset;
            result_y = "top";
          }
        }

        if (this.state.position == 'bottom left') {
          top = this_y + this_height + this.state.offset;
          left = this_x - popup_width + 22 + this_width / 2;

          result_y = "bottom";
          result_x = "left";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.offset;
            result_y = "top";
          }
        }

        if (this.state.position == 'bottom right') {
          top = this_y + this_height + this.state.offset;
          left = this_x + this_width - 22 - this_width / 2;

          result_y = "bottom";
          result_x = "right";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.offset;
            result_y = "top";
          }
        }

        if (this.state.position == 'left') {
          top = this_y - popup_height / 2 + this_height / 2;
          left = this_x - popup_width - this.state.offset;

          result_y = "center";
          result_x = "left";
          result_alter_y = "center";

          if (left < this.state.padding_window) {
            left = this_x + this_width + this.state.offset;
            result_x = "right";
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y - this_height + this.state.padding_window;
            result_alter_y = "bottom";
          }
          else if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height + this_height - this.state.padding_window;
            result_alter_y = "top";
          }
        }

        if (this.state.position == 'right') {
          top = this_y - popup_height / 2 + this_height / 2;
          left = this_x + this_width + this.state.offset;

          result_y = "center";
          result_x = "right";
          result_alter_y = "center";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.offset;
            result_x = "left";
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y - this_height + this.state.padding_window;
            result_alter_y = "bottom";
          }
          else if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height + this_height - this.state.padding_window;
            result_alter_y = "top";
          }
        }

        if (result_y == 'bottom') {
          if (result_x == 'center') {
            popup.addClass('bottom');
          }
          else if (result_x == 'left') {
            popup.addClass('bottom-left');
          }
          else if (result_x == 'right') {
            popup.addClass('bottom-right');
          }
        }
        else if (result_y == 'top') {
          if (result_x == 'center') {
            popup.addClass('top');
          }
          else if (result_x == 'left') {
            popup.addClass('top-left');
          }
          else if (result_x == 'right') {
            popup.addClass('top-right');
          }
        }
        else if (result_y == 'center') {
          if (result_x == 'center') {
          }
          else if (result_x == 'left') {
            popup.addClass('left');
          }
          else if (result_x == 'right') {
            popup.addClass('right');
          }
        }

        popup.offset({top: top, left: left});
      }
    }
  },

  render: function () {
    var child = [];
    var popup = null;

    if (this.state.popup) {
      var finalClassName = "popup";
      if (this.state.popup.className) {
        finalClassName += " " + this.state.popup.className;
      }
      popup = (<div ref="popup" className={finalClassName} key={this.state.popup.guid}>{this.state.popup.html}</div>);
    }

    var modal = null;

    if (this.state.modal) {
      modal = (<div ref="modal" className="modal" key={this.state.modal.guid}>{this.state.modal.html}</div>);
    }

    var back = null;

    if (modal) {
      child.push(modal);
      back = <div className="fullback" key="back" onClick={this.hideModal}></div>
    }
    else if (popup) {
      if (this.state.back)
      {
        if (this.state.element)
        {
          this.state.element.addClass('z-index-101');
        }
        back = <div className="fullback" key="back" onClick={this.hidePopup}></div>
      }
      child.push(popup);
    }
    else if (this.state.element) {
      this.state.element.removeClass('z-index-101');
    }

    return (
      <div>
        <ReactCSSTransitionGroup
          component="div"
          exclusive={false}
          transitionName="fade"
          transitionAppear={true}
          transitionAppearTimeout={300}
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}>
          {back}
        </ReactCSSTransitionGroup>
        <ReactCSSTransitionGroup
          component="div"
          exclusive={false}
          transitionName="fade-scale"
          transitionAppear={true}
          transitionAppearTimeout={300}
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}>
          {child}
        </ReactCSSTransitionGroup>
      </div>
    );
  }

});
