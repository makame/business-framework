loadingService = {};

LoadingContainer = React.createClass({

  getInitialState: function() {
    return {
      show: false,
    }
  },

  componentWillMount(){
    var self = this;

    loadingService.show = function(full) {
      self.state.show = true;
      self.setState({show: self.state.show});
    };

    loadingService.hide = function() {
      self.state.show = false;
      self.setState({show: self.state.show});
    };
  },

  // shouldComponentUpdate: function(nextProps, nextState) {
  //   if (nextState.show != this.state.show) {
  //     return true
  //   }
  //   return false
  // },

  render: function () {
    var props = {};

    var loadingClassName = "loading"

    if (this.state.show) {
      loadingClassName += " show"
    }

    var bigLoading = (
      <div className={loadingClassName}>
        <div className="loading-container">
          <div className="circle-1">
            <div className="circle-2">
              <div className="circle-3">
                <div className="circle-4">
                  <div className="circle-5">
                    <div className="circle-6">
                      <div className="circle-7">
                        <div className="circle-8">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="loading-container-label">
          [LOADING]
        </div>
      </div>
    )

    var loading = (
      <div className={loadingClassName}>
        <div className="loading-bar"></div>
      </div>
    )

    return loading;
  }
});
