Icon = React.createClass({
  render() {

    var icon = null
    if (this.props.name) {
      icon = <span className={"fa fa-" + this.props.name}></span>
    }

    return icon
  }
});
