var express = require('express');
app = express();
var http = require('http');
_ = require('underscore');

r = require('rethinkdbdash')({
  // port: 29015,
  // host: 'localhost',
  db: 'bustest',
  discovery: true,
  // ssl: true
});

// mysql = require('mysql');
// connection = mysql.createConnection({
//   host     : '127.0.0.1',
//   user     : 'root',
//   password : 'vfrfv',
//   database : 'bus_test'
// });

// connection.connect();

// require("node-jsx").install();

var session = require('express-session')

const RDBStore = require('session-rethinkdb')(session);
const store = new RDBStore({
  servers: [
    {host: 'localhost', port: 28015}
  ],
  db: 'bustest',
  clearInterval: 5000, // optional, default is 60000 (60 seconds). Time between clearing expired sessions.
  table: 'session' // optional, default is 'session'
});

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

var sessionMiddleware = session({
  // genid: function(req) {
  //   function s4() {
  //     return Math.floor((1 + Math.random()) * 0x10000)
  //     .toString(16)
  //     .substring(1);
  //   }
  //   return s4() + s4() + s4() + s4();
  // },
  store: store,
  resave: true,
  saveUninitialized: true,
  cookie: {
    // secure: true,
    maxAge: 24*60*60*1000,
    path: '/',
    httpOnly: false,
    cookie: 'session',
  },
  secret: 'business_fw'
})

app.use(sessionMiddleware)

app.use(express.static(__dirname + '/public'));

var React = require('react');
var ReactDOMServer = require('react-dom/server');
var defaults = require('lodash.defaults');
var path = require('path');

var reactEngine = function (engineOptions) {
  engineOptions = defaults(engineOptions || {}, {
    extension: '.jsx',
    babel: {
      ignore: false,
      extensions: [".jsx"],
      plugins: ["transform-react-jsx"],
    },
    docType: '<!DOCTYPE html>',
    staticMarkup: false
  });

  if (engineOptions.babel) {
    require('babel-register')(engineOptions.babel);
  }

  return function renderComponent (filename, options, callback) {
    options = options || {};

    try {
      var markup = engineOptions.docType;
      var Component = require(filename);
      var instance = React.createElement(Component, options);

      var componentMarkup;
      if (engineOptions.staticMarkup) {
        componentMarkup = ReactDOMServer.renderToStaticMarkup(instance);
      } else {
        componentMarkup = ReactDOMServer.renderToString(instance);
      }

      if (engineOptions.wrapper) {
        var Wrapper = require(path.join(this.root, engineOptions.wrapper));
        var wrapperInstance = React.createElement(Wrapper, {
          body: componentMarkup,
          props: options
        });

        markup += ReactDOMServer.renderToStaticMarkup(wrapperInstance);

      } else {
        markup += componentMarkup;
      }

      callback(null, markup);
    } catch (error) {
      callback(error);
    }
  };
};

app.engine('jsx', reactEngine({wrapper: 'html.jsx'}));
app.set('views', __dirname + '/components');

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Business app listening at http://%s:%s', host, port);
});

sock = require('socket.io')(server);

var sockOptions = {}

connected = [];

var requireDir = require('require-dir');
var libs = requireDir(__dirname + '/lib');
var serverTable = requireDir(__dirname + '/table');
var serverView = requireDir(__dirname + '/view');
// var serverSide = requireDir(__dirname + '/server');

Fiber = require('fibers');

sock.use(function(socket, next) {
  sessionMiddleware(socket.request, socket.request.res, next);
});

require('./modules/socket.js');

// Menu = [
//   {
//     type: 'drop',
//     value: [
//       {
//         type: 'view',
//         value: clientsView,
//         params: null,
//       }
//     ]
//   }
// ];

Clients.table().filter({}).delete().run().then(function(result) {
  Clients.table().insert({first_name: 'Gruzin', last_name: 'Bagal'}).run();
  Clients.table().insert({first_name: 'Alc', last_name: 'Bagdan'}).run();
});

require('./modules/route.js');
