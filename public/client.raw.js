guid = function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
  }
  return s4() + s4() + s4() + s4();
}

socket = io.connect();
LinkedStateMixin = require('react-addons-linked-state-mixin');
_ = require('underscore');

React = require('react');
ReactDOM = require('react-dom/server');
ReactCSSTransitionGroup = require('react-addons-css-transition-group');

Icon = React.createClass({displayName: "Icon",
  render() {

    var icon = null
    if (this.props.name) {
      icon = React.createElement("span", {className: "fa fa-" + this.props.name})
    }

    return icon
  }
});

Accord = React.createClass({displayName: "Accord",

  getInitialState: function() {
    return {
      show: false,
    }
  },

  toggleAccord: function(event) {
    this.state.show = !this.state.show
    this.setState(this.state)
  },

  render() {
    var status_icon = (
      React.createElement(Icon, {name: "plus-square-o"})
    )

    if (this.state.show) {
      status_icon = (
        React.createElement(Icon, {name: "minus-square-o"})
      )
    }

    var child = null

    if (this.state.show) {
      child = (
        React.createElement("div", {id: "child", className: "accord"}, 
          this.props.children
        )
      )
    }

    return (
      React.createElement("div", {className: "accord container block"}, 
        React.createElement("div", {className: "accord-header align-right padding-horizontal-2 padding-vertical-1 pointer", onClick: this.toggleAccord}, 
          status_icon, 
          this.props.header, 
          React.createElement(Icon, {name: this.props.icon})
        ), 
        child
      )
    )
  }
});

view_subs = {}

socket.on('view_changes', function(data) {
  subs = view_subs[data.name];
  if (view_subs[data.name]) {
    view_subs[data.name].func(data.data, view_subs[data.name].valueLink);
  }
});

BusinessData = {

  subscribe_view: function(name, func, valueLink) {
    var temp = valueLink.value
    temp.loading_view = true
    loadingService.show()
    valueLink.requestChange(temp)

    view_subs[name] = {func: func, valueLink: valueLink}
    data = {
      name: name,
      order: valueLink.value.order,
      skip: valueLink.value.skip,
      limit: valueLink.value.limit,
      filters: valueLink.value.filters,
      order_desc: valueLink.value.order_desc,
    }
    socket.emit('subscribe_view', data);
  },

  desubscribe_view: function(name) {
    subs = view_subs[name]
    if (view_subs[name]) {
      delete view_subs[name]
    }
    socket.emit('desubscribe_view', {name: name});
  },

  orderViewData: function(temp) {
    temp.data = _.sortBy(temp.data, function(item) {
      return item[temp.order]
    })
    if (temp.order_desc) {
      temp.data = temp.data.reverse()
    }
    return temp
  },

  receiveViewData: function(data, valueLink) {
    if (data.state == 'preinitializing') {
      var temp = valueLink.value
      temp.fields = data.fields
      temp.order = data.order
      temp.skip = data.skip
      temp.limit = data.limit
      temp.order_desc = data.order_desc
      temp.loading_view = true
      loadingService.show()
      temp.filters = _.map(data.filters, function(item, index) {
        return _.extend(item, valueLink.value.filters[index])
      })
      valueLink.requestChange(temp)
    }
    else if (data.state == 'initializing') {
      var temp = valueLink.value
      temp.temp_data = []
      // temp.data = []
      temp.loading_view = true
      loadingService.show()
      valueLink.requestChange(temp)
    }
    else if (data.state == 'additional') {
      var temp = valueLink.value
      temp.count = data.count
      valueLink.requestChange(temp)
    }
    else if (data.state == 'ready') {
      var temp = valueLink.value
      temp.loading_view = false
      loadingService.hide()
      temp.data = temp.temp_data
      temp = this.orderViewData(temp)
      valueLink.requestChange(temp)
    }
    else {
      if (!!data.new_val && !data.old_val) {
        var temp = valueLink.value
        if (temp.loading_view) {
          var finded = false
          for (var i = 0; i < valueLink.value.data.length; i++) {
            if (!!temp.temp_data[i] && temp.temp_data[i].id == data.new_val.id) {
              temp.temp_data[i] = _.extend(data.new_val, temp.temp_data[i])
              finded = true
              break;
            }
          }
          if (!finded) {
            temp.temp_data.push(data.new_val)
          }
        }
        else {
          temp.data.push(data.new_val)
          temp = this.orderViewData(temp)
        }
        valueLink.requestChange(temp)
      }
      else if (!!data.new_val && !!data.old_val) {
        var temp = valueLink.value
        if (temp.loading_view) {
          for (var i = 0; i < valueLink.value.data.length; i++) {
            if (temp.temp_data[i].id == data.new_val.id) {
              temp.temp_data[i] = _.extend(data.new_val, temp.temp_data[i])
              break;
            }
          }
          valueLink.requestChange(temp)
        }
        else {
          for (var i = 0; i < valueLink.value.data.length; i++) {
            if (temp.data[i].id == data.new_val.id) {
              temp.data[i] = _.extend(data.new_val, temp.data[i])
              break;
            }
          }
          temp = this.orderViewData(temp)
          valueLink.requestChange(temp)
        }
      }
      else if (!data.new_val && !!data.old_val) {
        var temp = valueLink.value
        if (temp.loading_view) {
          for (var i = 0; i < temp.temp_data.length; i++) {
            if (temp.temp_data[i].id == data.old_val.id) {
              temp.temp_data.splice(i, 1)
              valueLink.requestChange(temp)
              break;
            }
          }
        }
        else {
          for (var i = 0; i < temp.data.length; i++) {
            if (temp.data[i].id == data.old_val.id) {
              temp.data.splice(i, 1)
              valueLink.requestChange(temp)
              break;
            }
          }
        }
      }
    }
  },
}

Input = React.createClass({displayName: "Input",
  handleChange: function(e) {
    this.props.linkState.requestChange(e.target.value);
  },

  onKeyUp: function(e) {
    if (e.which == 13) {
      if (this.props.onEnter) {
        this.props.onEnter()
      }
    }
  },

  render() {
    return (
      React.createElement("input", {type: this.props.type, value: this.props.linkState.value, onChange: this.handleChange, onKeyUp: this.onKeyUp})
    )
  }
});

loadingService = {};

LoadingContainer = React.createClass({displayName: "LoadingContainer",

  getInitialState: function() {
    return {
      show: false,
    }
  },

  componentWillMount(){
    var self = this;

    loadingService.show = function(full) {
      self.state.show = true;
      self.setState({show: self.state.show});
    };

    loadingService.hide = function() {
      self.state.show = false;
      self.setState({show: self.state.show});
    };
  },

  // shouldComponentUpdate: function(nextProps, nextState) {
  //   if (nextState.show != this.state.show) {
  //     return true
  //   }
  //   return false
  // },

  render: function () {
    var props = {};

    var loadingClassName = "loading"

    if (this.state.show) {
      loadingClassName += " show"
    }

    var bigLoading = (
      React.createElement("div", {className: loadingClassName}, 
        React.createElement("div", {className: "loading-container"}, 
          React.createElement("div", {className: "circle-1"}, 
            React.createElement("div", {className: "circle-2"}, 
              React.createElement("div", {className: "circle-3"}, 
                React.createElement("div", {className: "circle-4"}, 
                  React.createElement("div", {className: "circle-5"}, 
                    React.createElement("div", {className: "circle-6"}, 
                      React.createElement("div", {className: "circle-7"}, 
                        React.createElement("div", {className: "circle-8"}
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        ), 
        React.createElement("div", {className: "loading-container-label"}, 
          "[LOADING]"
        )
      )
    )

    var loading = (
      React.createElement("div", {className: loadingClassName}, 
        React.createElement("div", {className: "loading-bar"})
      )
    )

    return loading;
  }
});

popupService = {};
modalService = {};

PopupElement = React.createClass({displayName: "PopupElement",

  getInitialState: function() {
    return {guid: guid()}
  },

  getDefaultProps: function() {
    return {on: 'hover', back: false, html: '', position: 'top center', popupClassName: '', offset: 9}
  },

  show: function(e) {
    var element = $(ReactDOM.findDOMNode(this));
    if (this.props.element) {
      element = $(ReactDOM.findDOMNode(this.props.element));
    }

    popupService.showPopup({html: this.props.html, guid: this.state.guid, className: this.props.popupClassName, offset: this.props.offset}, element, this.props.back, this.props.position);
  },

  hide: function(e) {
    popupService.hidePopup(this.state.guid);
  },

  componentWillReceiveProps: function(nextProps) {
    this.props = nextProps;
    if (nextProps.on) {
      if (nextProps.show) {
        this.show();
      }
      else {
        this.hide();
      }
    }
    this.setState({on: nextProps.on});
  },

  componentDidMount: function() {
    this.setState({on: this.props.on});
  },

  render: function () {
    if (this.state.on == "manual") {
      return (React.createElement("div", {className: this.props.className}, this.props.children));
    }
    else if (this.state.on == "click") {
      return (React.createElement("div", {onClick: this.show, onTouchStart: this.show, className: this.props.className}, this.props.children));
    }
    else if (this.state.on == "hover") {
      return (React.createElement("div", {onMouseOver: this.show, onMouseOut: this.hide, onTouchStart: this.show, onTouchEnd: this.hide, className: this.props.className}, this.props.children));
    }
    else {
      return null;
    }
  }

});

ModalElement = React.createClass({displayName: "ModalElement",

  guid: guid(),

  show: function(e) {
    e.stopPropagation();
    modalService.showModal({html: this.props.html, guid: this.guid, offset: this.props.offset});
  },

  hide: function(e) {
    modalService.hideModal(this.guid);
  },

  render: function () {
    return (React.createElement("div", {onClick: this.show, onTouchStart: this.show, className: this.props.className}, this.props.children));
  }

});

WindowContainer = React.createClass({displayName: "WindowContainer",

  componentWillMount(){
    var self = this;

    popupService.showPopup = function(popup, element, back, position) {
      if (!back) {
        back = false;
      }

      var offset = 9;

      if (popup && popup.offset != null) {
        offset = popup.offset;
      }

      self.setState({popup: popup, element, back, position, offset: offset});
    };

    popupService.hidePopup = function(guid) {
      if (guid) {
        if (self.state.popup && self.state.popup.guid == guid) {
          self.state.popup = null;
          self.setState({popup: self.state.popup, element: null});
        }
      }
      else {
        self.state.popup = null;
        self.setState({popup: self.state.popup, element: null});
      }
    };

    modalService.showModal = function(modal) {
      self.setState({modal: modal});
    };

    modalService.hideModal = function(guid) {
      self.state.modal = null;
      self.setState({modal: self.state.modal});
    };
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true
  },

  hideModal: function(e) {
    this.state.modal = null;
    this.setState({modal: this.state.modal});
  },

  hidePopup: function(e) {
    this.state.popup = null;
    this.setState({popup: this.state.popup, element: null});
  },

  hideWindow: function(e) {
    this.state.window = null;
    this.setState({window: this.state.window, element: null});
  },

  getInitialState: function() {
    return {modal: null, popup: null, element: null, padding_window: 10, position: 'top center', offset: 9};
  },

  componentDidUpdate: function() {
    if (this.state.modal) {
      var modal = $(ReactDOM.findDOMNode(this.refs.modal));

      var min_x = $(window).scrollLeft();
      var min_y = $(window).scrollTop();

      var max_x = $(window).width() + $(window).scrollLeft();
      var max_y = $(window).height() + $(window).scrollTop();

      var modal_height = modal.outerHeight();
      var modal_width = modal.outerWidth();

      var top = min_y + (max_y - min_y) / 2 - modal_height / 2;
      var left = min_x + (max_x - min_x) / 2 - modal_width / 2;

      modal.offset({top: top, left: left});
    }

    if (this.state.popup) {
      var popup = $(ReactDOM.findDOMNode(this.refs.popup));
      if (popup) {
        var element = this.state.element;

        var min_x = $(window).scrollLeft();
        var min_y = $(window).scrollTop();

        var max_x = $(window).width() + $(window).scrollLeft();
        var max_y = $(window).height() + $(window).scrollTop();

        var this_y = element.offset().top;
        var this_x = element.offset().left;

        var this_height = element.outerHeight();
        var this_width = element.outerWidth();

        var popup_height = popup.outerHeight();
        var popup_width = popup.outerWidth();

        var top = this_y;
        var left = this_x;

        var want_y = 'auto';
        var want_x = 'auto';

        var result_y = 'auto';
        var result_x = 'auto';
        var result_alter_y = 'center';

        if (this.state.position == 'top center' || this.state.position == 'top') {
          top = this_y - popup_height - this.state.offset;
          left = this_x + this_width / 2 - popup_width / 2;

          result_y = "top";
          result_x = "center";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y + this_height + this.state.offset;
            result_y = "bottom";
          }
        }

        if (this.state.position == 'top left') {
          top = this_y - popup_height - this.state.offset;
          left = this_x - popup_width + 22 + this_width / 2;

          result_y = "top";
          result_x = "left";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top < this_height - this.state.padding_window) {
            top = this_y + this_height + this.state.offset;
            result_y = "bottom";
          }
        }

        if (this.state.position == 'top right') {
          top = this_y - popup_height - this.state.offset;
          left = this_x + this_width - 22 - this_width / 2;

          result_y = "top";
          result_x = "right";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top < this_height - this.state.padding_window) {
            top = this_y + this_height + this.state.offset;
            result_y = "bottom";
          }
        }

        if (this.state.position == 'bottom center' || this.state.position == 'bottom') {
          top = this_y + this_height + this.state.offset;
          left = this_x + this_width / 2 - popup_width / 2;

          result_y = "bottom";
          result_x = "center";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.offset;
            result_y = "top";
          }
        }

        if (this.state.position == 'bottom left') {
          top = this_y + this_height + this.state.offset;
          left = this_x - popup_width + 22 + this_width / 2;

          result_y = "bottom";
          result_x = "left";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.offset;
            result_y = "top";
          }
        }

        if (this.state.position == 'bottom right') {
          top = this_y + this_height + this.state.offset;
          left = this_x + this_width - 22 - this_width / 2;

          result_y = "bottom";
          result_x = "right";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window;
            result_x = "left";
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window;
            result_x = "right";
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.offset;
            result_y = "top";
          }
        }

        if (this.state.position == 'left') {
          top = this_y - popup_height / 2 + this_height / 2;
          left = this_x - popup_width - this.state.offset;

          result_y = "center";
          result_x = "left";
          result_alter_y = "center";

          if (left < this.state.padding_window) {
            left = this_x + this_width + this.state.offset;
            result_x = "right";
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y - this_height + this.state.padding_window;
            result_alter_y = "bottom";
          }
          else if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height + this_height - this.state.padding_window;
            result_alter_y = "top";
          }
        }

        if (this.state.position == 'right') {
          top = this_y - popup_height / 2 + this_height / 2;
          left = this_x + this_width + this.state.offset;

          result_y = "center";
          result_x = "right";
          result_alter_y = "center";

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.offset;
            result_x = "left";
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y - this_height + this.state.padding_window;
            result_alter_y = "bottom";
          }
          else if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height + this_height - this.state.padding_window;
            result_alter_y = "top";
          }
        }

        if (result_y == 'bottom') {
          if (result_x == 'center') {
            popup.addClass('bottom');
          }
          else if (result_x == 'left') {
            popup.addClass('bottom-left');
          }
          else if (result_x == 'right') {
            popup.addClass('bottom-right');
          }
        }
        else if (result_y == 'top') {
          if (result_x == 'center') {
            popup.addClass('top');
          }
          else if (result_x == 'left') {
            popup.addClass('top-left');
          }
          else if (result_x == 'right') {
            popup.addClass('top-right');
          }
        }
        else if (result_y == 'center') {
          if (result_x == 'center') {
          }
          else if (result_x == 'left') {
            popup.addClass('left');
          }
          else if (result_x == 'right') {
            popup.addClass('right');
          }
        }

        popup.offset({top: top, left: left});
      }
    }
  },

  render: function () {
    var child = [];
    var popup = null;

    if (this.state.popup) {
      var finalClassName = "popup";
      if (this.state.popup.className) {
        finalClassName += " " + this.state.popup.className;
      }
      popup = (React.createElement("div", {ref: "popup", className: finalClassName, key: this.state.popup.guid}, this.state.popup.html));
    }

    var modal = null;

    if (this.state.modal) {
      modal = (React.createElement("div", {ref: "modal", className: "modal", key: this.state.modal.guid}, this.state.modal.html));
    }

    var back = null;

    if (modal) {
      child.push(modal);
      back = React.createElement("div", {className: "fullback", key: "back", onClick: this.hideModal})
    }
    else if (popup) {
      if (this.state.back)
      {
        if (this.state.element)
        {
          this.state.element.addClass('z-index-101');
        }
        back = React.createElement("div", {className: "fullback", key: "back", onClick: this.hidePopup})
      }
      child.push(popup);
    }
    else if (this.state.element) {
      this.state.element.removeClass('z-index-101');
    }

    return (
      React.createElement("div", null, 
        React.createElement(ReactCSSTransitionGroup, {
          component: "div", 
          exclusive: false, 
          transitionName: "fade", 
          transitionAppear: true, 
          transitionAppearTimeout: 300, 
          transitionEnterTimeout: 300, 
          transitionLeaveTimeout: 300}, 
          back
        ), 
        React.createElement(ReactCSSTransitionGroup, {
          component: "div", 
          exclusive: false, 
          transitionName: "fade-scale", 
          transitionAppear: true, 
          transitionAppearTimeout: 300, 
          transitionEnterTimeout: 300, 
          transitionLeaveTimeout: 300}, 
          child
        )
      )
    );
  }

});

gridPage = React.createClass({displayName: "gridPage",
  mixins: [LinkedStateMixin, BusinessData],

  getInitialState: function() {
    return {
      grid_data: {
        loading_view: true,
        data: [],
        fields: [],
        order: null,
        order_desc: false,
        skip: 0,
        limit: 10,
        filters: [],
        count: 0,
      },
      page: 1,
      per_page: 10,
      selected_row: [],
    }
  },

  do_subscribe: function() {
    this.state.grid_data.limit = this.state.page * this.state.per_page
    this.state.grid_data.skip = (this.state.page - 1) * this.state.per_page
    this.setState(this.state.grid_data)
    this.subscribe_view(this.props.data.name, this.receiveViewData, this.linkState('grid_data'))
  },

  go_page: function(page) {
    if (page < 1) {
      return
    }

    if (page > Math.ceil(this.state.grid_data.count / this.state.per_page)) {
      return
    }

    if (this.state.grid_data.loading_view) {
      return
    }

    this.state.page = page
    this.setState({grid_data: this.state.grid_data, page: this.state.page})
    this.do_subscribe()
  },

  load_more: function() {
    if (this.state.per_page > this.state.grid_data.count) {
      return
    }

    if (this.state.grid_data.loading_view) {
      return
    }

    this.state.per_page = this.state.per_page + 10
    this.state.page = 1
    this.state.grid_data.limit = this.state.page * this.state.per_page
    this.state.grid_data.skip = (this.state.page - 1) * this.state.per_page
    this.setState({per_page: this.state.per_page, page: this.state.page, grid_data: this.state.grid_data})
    this.do_subscribe()
  },

  load_less: function() {
    if (this.state.per_page <= 10) {
      return
    }

    if (this.state.grid_data.loading_view) {
      return
    }

    this.state.per_page = this.state.per_page - 10
    this.state.page = 1
    this.state.grid_data.limit = this.state.page * this.state.per_page
    this.state.grid_data.skip = (this.state.page - 1) * this.state.per_page
    this.setState({per_page: this.state.per_page, page: this.state.page, grid_data: this.state.grid_data})
    this.do_subscribe()
  },

  isAltDown: false,

  componentDidMount: function() {
    var self = this
    document.onkeydown = function(e) {
      switch (e.keyCode) {
        case 18:
        self.isAltDown = true
        break;
        case 37:
        if (self.isAltDown) {
          self.go_page(self.state.page - 1)
        }
        break;
        case 39:
        if (self.isAltDown) {
          self.go_page(self.state.page + 1)
        }
        break;
        case 38:
        if (self.isAltDown) {
          self.load_less()
        }
        break;
        case 40:
        if (self.isAltDown) {
          self.load_more()
        }
        break;
      }
    }

    document.onkeyup = function(e) {
      switch (e.keyCode) {
        case 18:
        self.isAltDown = false
        break;
      }
    }

    this.do_subscribe()
  },

  render: function () {
    var self = this
    var items = (React.createElement(ItemsPopup, null))

    var onClick = function() {
      self.do_subscribe()
    }

    var table_heads = [];

    table_heads.push(
      React.createElement("td", {key: "checkbox"})
    )

    _.each(this.state.grid_data.fields, function(field) {

      if (field.sortable) {
        var order_icon = null

        if (self.state.grid_data.order == field.name && self.state.grid_data.order_desc) {
          order_icon = (
            React.createElement("span", {className: "fa fa-sort-amount-desc"})
          )
        }
        else if (self.state.grid_data.order == field.name && !self.state.grid_data.order_desc) {
          order_icon = (
            React.createElement("span", {className: "fa fa-sort-amount-asc"})
          )
        }

        var reOrder = function() {
          if (self.state.grid_data.order == field.name && !self.state.grid_data.order_desc) {
            self.state.grid_data.order_desc = true
          }
          else {
            self.state.grid_data.order_desc = false
          }
          self.state.grid_data.order = field.name

          self.setState(self.state.grid_data)

          self.do_subscribe()
        }

        table_heads.push(
          React.createElement("td", {className: "pointer", key: field.name, onClick: reOrder}, field.label, " ", order_icon)
        )
      }
      else {
        table_heads.push(
          React.createElement("td", {key: field.name}, field.label)
        )
      }
    })

    var table_items = [];
    var data = this.state.grid_data.data

    // if (this.state.grid_data.data.length > this.state.grid_data.skip) {
    data = data.slice(this.state.grid_data.skip, this.state.grid_data.skip + this.state.per_page)
    // }

    _.each(data, function(item, index) {
      var tds = []

      var linkCheck = {
        value: item.checkbox,
        requestChange: function(newValue) {
          self.state.grid_data.data[index].checkbox = newValue;
          self.setState(self.state.grid_data.data[index].checkbox);
        }
      }

      tds.push(React.createElement("td", {key: "checkbox"}, React.createElement("input", {type: "checkbox", linkState: linkCheck})))

      _.each(self.state.grid_data.fields, function(field) {
        if (field.type == 'string') {
          tds.push(React.createElement("td", {key: field.name}, item[field.name]))
        }
        else if (field.type == 'refs') {
          //{item[field.name].data.length}
          tds.push(React.createElement("td", {key: field.name, className: "ellipsis"}, item[field.name].data.length))
        }
      })

      table_items.push(
        React.createElement("tr", {key: item.id}, 
          tds
        )
      )
    })

    var table = (
      React.createElement("table", {className: "table container p95 center", key: "table"}, 
        React.createElement("thead", null, 
          React.createElement("tr", null, 
            table_heads
          )
        ), 
        React.createElement("tbody", null, 
          table_items
        )
      )
    )

    var filters = []

    _.each(self.state.grid_data.filters, function(item, index) {
      if (item.type == "regexp") {

        var link = {
          value: item.value,
          requestChange: function(newValue) {
            item.value = newValue;
            self.setState(self.state.grid_data);
          }
        }

        filters.push(
          React.createElement("tr", {key: index}, 
            React.createElement("td", {className: "type-1 align-right width-1 nowrap"}, item.label), 
            React.createElement("td", {className: "type-1"}, React.createElement(Input, {type: "text", linkState: link, onEnter: self.do_subscribe}))
          )
        )
      }
    })

    var stat = (
      React.createElement("div", {className: "container block align-center margin-top-2"}, 
        "Showed ", self.state.grid_data.data.length, " of ", self.state.grid_data.count, " & Skiped ", self.state.grid_data.skip
      )
    )

    var pagination_item = []

    var max_page = Math.ceil(self.state.grid_data.count / self.state.per_page)

    if (self.state.page > 1) {
      var goBack = function() {
        self.go_page(self.state.page - 1)
      }

      pagination_item.push(React.createElement("div", {onClick: goBack, key: "back"}, "(Alt + Left)"))
    }

    if (self.state.page > 1) {
      var goFirst = function() {
        self.go_page(1)
      }

      pagination_item.push(React.createElement("div", {onClick: goFirst, key: "first"}, "1"))
    }

    if (self.state.page > 2) {
      pagination_item.push(React.createElement("div", {className: "disabled", key: "pre"}, ".."))
    }

    pagination_item.push(React.createElement("div", {className: "active disabled", key: "current"}, self.state.page))

    if (self.state.page < max_page - 1) {
      pagination_item.push(React.createElement("div", {className: "disabled", key: "after"}, ".."))
    }

    if (self.state.page < max_page) {
      var goMax = function() {
        self.go_page(max_page)
      }

      pagination_item.push(React.createElement("div", {onClick: goMax, key: "max"}, max_page))
    }

    if (self.state.page < max_page) {
      var goNext = function() {
        self.go_page(self.state.page + 1)
      }

      pagination_item.push(React.createElement("div", {onClick: goNext, key: "next"}, "(Alt + Right)"))
    }

    var pagination = (
      React.createElement("div", {className: "buttons container block align-center margin-top-2"}, 
        pagination_item
      )
    )

    if (max_page == 1) {
      pagination = null
    }

    var load_more_func = this.load_more
    var load_more_class = ""

    if (this.state.grid_data.loading_view) {
      load_more_func = null
      load_more_class = "disabled"
    }

    var load_more = (
      React.createElement("div", {className: "buttons container block align-center margin-top-2"}, 
        React.createElement("div", {className: load_more_class, onClick: load_more_func}, "Load More")
      )
    )

    if (this.state.per_page > this.state.grid_data.count || this.state.page > 1) {
      load_more = null
    }

    var popup = (React.createElement(ConfidentialPopup, null))

    return (
      React.createElement("div", {className: "padding-bottom-2"}, 
        React.createElement("div", {className: "top-menu container p90 block center"}, 
          React.createElement("div", {className: "header"}, 
            "Your Company Name"
          ), 
          React.createElement("div", {className: "items-desktop"}, 
            React.createElement("div", {className: "item"}, 
              "Orders"
            ), 
            React.createElement("div", {className: "item"}, 
              "Clients"
            ), 
            React.createElement("div", {className: "item"}, 
              "Products"
            )
          ), 
          React.createElement("div", {className: "items-mobile"}, 
            React.createElement(PopupElement, {html: items, position: "bottom center", offset: -0, className: "item", on: "click", back: true}, 
              React.createElement("span", {className: "fa fa-bars"}), 
              "Menu"
            )
          ), 
          React.createElement("div", {className: "space"}
          ), 
          React.createElement(PopupElement, {html: popup, position: "bottom left", offset: -0, className: "item", on: "click", back: true}, 
            React.createElement("span", {className: "fa fa-cog"}), 
            "System"
          )
        ), 

        React.createElement(Accord, {header: "Filters", icon: "filter"}, 
          React.createElement("div", {className: "padding-horizontal-2 padding-vertical-1"}, 
            React.createElement("table", {className: "table"}, 
              React.createElement("tbody", null, 
                filters
              )
            )
          )
        ), 

        table, 

        stat, 

        load_more, 

        pagination
      )
    );
  }
});

ItemsPopup = React.createClass({displayName: "ItemsPopup",
  render() {
    return (
      React.createElement("div", {className: "popup-menu"}, 
        React.createElement("div", {className: "item"}, 
          "Orders"
        ), 
        React.createElement("div", {className: "item"}, 
          "Clients"
        ), 
        React.createElement("div", {className: "item"}, 
          "Products"
        )
      )
    )
  }
});

ConfidentialPopup = React.createClass({displayName: "ConfidentialPopup",
  render() {
    var popup = (React.createElement(ConfidentialModal, null))
    return (
      React.createElement("div", {className: "padding-vertical-2 padding-horizontal-2"}, 
        React.createElement("p", null, 
          "All information will stay confidential and will not be transfered to the third parties. We do not store any of you application data after the moderation process. We do not store any logs IP's or any of your provider information."
        ), 
        React.createElement(ModalElement, {html: popup, className: "a"}, 
          "Privacy Policy"
        )
      )
    )
  }
});

ConfidentialModal = React.createClass({displayName: "ConfidentialModal",
  render() {
    return (
      React.createElement("div", {className: "container litle padding-vertical-4 padding-horizontal-4"}, 
        React.createElement("div", null, 
          React.createElement("p", null, 
            "All information will stay confidential and will not be transfered to the third parties. We do not store any of you application data after the moderation process. We do not store any logs IP's or any of your provider information."
          ), 
          React.createElement("div", null, 
            React.createElement("a", null, "Privacy Policy")
          )
        )
      )
    )
  }
});

homePage = React.createClass({displayName: "homePage",
  render: function () {
    var popup = (React.createElement(ConfidentialPopup, null))
    var items = (React.createElement(ItemsPopup, null))
    console.log(this.props.data);
    return (
      React.createElement("div", null, 
      React.createElement("div", {className: "top-menu"}, 
        React.createElement("div", {className: "header"}, 
          "Your Company Name"
        ), 
        React.createElement("div", {className: "items"}, 
          React.createElement("div", {className: "item"}, 
            "Orders"
          ), 
          React.createElement("div", {className: "item"}, 
            "Clients"
          ), 
          React.createElement("div", {className: "item"}, 
            "Products"
          )
        ), 
        React.createElement(PopupElement, {html: items, position: "bottom center", offset: -0, className: "item more", on: "click", back: true}, 
          React.createElement("span", {className: "fa fa-bars"}), 
          "Menu"
        ), 
        React.createElement("div", {className: "space"}
        ), 
        React.createElement(PopupElement, {html: popup, position: "bottom left", offset: -0, className: "item", on: "click", back: true}, 
          React.createElement("span", {className: "fa fa-cog"}), 
          "System"
        )
      ), 
      React.createElement("div", null, 
        "Hello"
      )
    )
    );
  }
});

ItemsPopup = React.createClass({displayName: "ItemsPopup",
  render() {
    return (
      React.createElement("div", {className: "popup-menu"}, 
        React.createElement("div", {className: "item"}, 
          "Orders"
        ), 
        React.createElement("div", {className: "item"}, 
          "Clients"
        ), 
        React.createElement("div", {className: "item"}, 
          "Products"
        )
      )
    )
  }
});

ConfidentialPopup = React.createClass({displayName: "ConfidentialPopup",
  render() {
    var popup = (React.createElement(ConfidentialModal, null))
    return (
      React.createElement("div", {className: "padding-vertical-2 padding-horizontal-2"}, 
        React.createElement("p", null, 
          "All information will stay confidential and will not be transfered to the third parties. We do not store any of you application data after the moderation process. We do not store any logs IP's or any of your provider information."
        ), 
        React.createElement(ModalElement, {html: popup, className: "a"}, 
          "Privacy Policy"
        )
      )
    )
  }
});

ConfidentialModal = React.createClass({displayName: "ConfidentialModal",
  render() {
    return (
      React.createElement("div", {className: "container litle padding-vertical-4 padding-horizontal-4"}, 
        React.createElement("div", null, 
          React.createElement("p", null, 
            "All information will stay confidential and will not be transfered to the third parties. We do not store any of you application data after the moderation process. We do not store any logs IP's or any of your provider information."
          ), 
          React.createElement("div", null, 
            React.createElement("a", null, "Privacy Policy")
          )
        )
      )
    )
  }
});

loginPage = React.createClass({displayName: "loginPage",
  render: function () {
    return (
      React.createElement("div", {className: "top-menu"}, 
        React.createElement("div", {className: "header"}, 
          "Your Company Name"
        ), 
        React.createElement("div", {className: "item"}, 
          "Please Login"
        ), 
        React.createElement("div", {className: "space"}
        ), 
        React.createElement("div", {className: "item"}, 
          "About"
        )
      )
    );
  }
});

var React = require('react');

Stater = React.createClass({displayName: "Stater",
  getCurrentPage: function () {
    console.log(this.props);

    var varialable = window[this.props.page + 'Page'];
    if (varialable) {
      var page = React.createElement(varialable, {source: this.props.source, data: this.props.data})
      return page;
    }
    else {
      return 'Not Found Module';
    }
  },

  render: function () {
    return (
      React.createElement("div", null, 
        this.getCurrentPage()
      )
    );
  }
});

var ReactDOM = require('react-dom');

ReactDOM.render(React.createElement(WindowContainer, null), document.getElementById('window-container'));

ReactDOM.render(React.createElement(LoadingContainer, null), document.getElementById('loading-container'));

var props = JSON.parse(document.getElementById("props").innerHTML);
ReactDOM.render(React.createElement(Stater, {page: props.page, source: props.source, data: props.data}), document.getElementById('view-client'));
