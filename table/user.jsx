Users = new Table({
  table_name: 'user',
  fields: {
    id: {
      label: 'ID',
      name: 'id',
      type: 'int',
    },
    nickname: {
      label: 'Nick Name',
      name: 'nickname',
      type: 'string',
    },
    password: {
      label: 'Password',
      name: 'password',
      type: 'string',
    },
  },
})

Sessions = new Table({
  table_name: 'session',
  fields: {
    id: {
      label: 'ID',
      name: 'id',
      type: 'int',
    },
    client_id: {
      label: 'User',
      name: 'user_id',
      type: 'ref',
    },
    secure_key_1: {
      label: 'Secure key 1',
      name: 'secure_key_1',
      type: 'string',
    },
    secure_key_2: {
      label: 'Secure key 2',
      name: 'secure_key_2',
      type: 'string',
    },
  },
  refs: [
    {
      table: Users,
      display: function (row) {
        return row.nickname
      },
      key: 'id',
    }
  ]
})
