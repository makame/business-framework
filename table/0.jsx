Table = function Table(init_data) {
  this._init_data = init_data;
  this.fields = init_data.fields;
  this.table_name = init_data.table_name;
}

Table.prototype.table = function() {
  return r.table(this.table_name);
}

Table.prototype.log = function() {
  console.log(this._init_data);
}
