Clients = new Table({
  table_name: 'client',
  fields: {
    id: {
      label: 'ID',
      name: 'id',
      type: 'string',
    },
    first_name: {
      label: 'First Name',
      name: 'first_name',
      type: 'string',
    },
    last_name: {
      label: 'Last Name',
      name: 'last_name',
      type: 'string',
    },
  },
})

Contacts = new Table({
  table_name: 'client_contact',
  fields: {
    id: {
      label: 'ID',
      name: 'id',
      type: 'string',
    },
    client_id: {
      label: 'Client',
      name: 'client_id',
      type: 'ref',
    },
    type: {
      label: 'Type',
      name: 'type',
      type: 'string',
    },
    value: {
      label: 'Value',
      name: 'value',
      type: 'string',
    },
  },
  refs: [
    {
      name: 'client_id',
      table: Clients,
      display: function (row) {
        return row.first_name + ' ' + row.last_name
      },
      key: 'id',
    }
  ]
})
